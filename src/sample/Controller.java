package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sample.backend.Traitement;
import sample.component.Popup;
import sample.modele.*;
import sample.reader.Reader;
import sample.util.Key;
import sample.writer.Writer;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;


public class Controller implements Initializable,EventHandler<ActionEvent> {

    @FXML
    private Button buttonVaisseau;

    @FXML
    private Button buttonItineraire;

    @FXML
    private Button button_vortex;

    @FXML
    private Button button_settings;

    @FXML
    private Button buttonAjoutFichierVortex;

    @FXML
    private TextArea textareaVaisseau;

    @FXML
    private ChoiceBox<String> choixStatistics;

    @FXML
    private Button buttonValidationStatistics;

    @FXML
    private Text titrePage;

    @FXML
    private GridPane layout_Itineraire;

    @FXML
    private GridPane layout_Vortex;

    @FXML
    private GridPane layout_settings;

    @FXML
    private GridPane layout_vaisseau;

    @FXML
    private BarChart<Object, Integer> grapheMeilleureShoot;

    @FXML
    private CategoryAxis graphShootDegatShip;

    @FXML
    private NumberAxis graphShootNameShip;

    @FXML
    private Button button_triangulation;

    @FXML
    private TableView<Unite> tableauMoyenne;

    @FXML
    private TableColumn<Unite, String> nomVaisseau;

    @FXML
    private TableColumn<Unite, String> vaisseauDetruitEtPerdu;

    @FXML
    private TableColumn<Unite, Integer> bouclierMoyenne;

    @FXML
    private TableColumn<Unite, Integer> BouclierShootMax;

    @FXML
    private TableColumn<Unite, Integer> bouclierDegatTotal;

    @FXML
    private TableColumn<Unite, Integer> coqueMoyenne;

    @FXML
    private TableColumn<Unite, Integer> coqueShootMax;

    @FXML
    private TableColumn<Unite, Integer> coqueDegatTotal;

    @FXML
    private TableColumn<Unite, Integer> degatTotalBouclierEtCoque;

    @FXML
    private Button buttonValidationVortexProche;

    @FXML
    private TextField textFieldRechercheVortexProche;
    ///
    @FXML
    private TextArea textAreaVortexProche;

    @FXML
    private Button buttonCalculerRoute;


    @FXML
    private TextField planet1;

    @FXML
    private TextField planet2;

    @FXML
    private TextField planet3;

    @FXML
    private TextField planetE11;

    @FXML
    private TextField planetE12;

    @FXML
    private TextField planetE13;

    @FXML
    private TextField planetE21;

    @FXML
    private TextField planetE22;

    @FXML
    private TextField planetE23;

    @FXML
    private TextField planetE31;

    @FXML
    private TextField planetE32;

    @FXML
    private TextField planetE33;

    @FXML
    private TextField planetE41;

    @FXML
    private TextField planetE42;

    @FXML
    private TextField planetE43;

    @FXML
    private TextField planetE51;

    @FXML
    private TextField planetE52;

    @FXML
    private TextField planetE53;

    @FXML
    private Button button_triangulation_planete;

    @FXML
    private HBox layout_planets;

    @FXML
    private Label Destination1;

    @FXML
    private Label Distance1;

    @FXML
    private TextArea textAreaVortex1Pc;

    @FXML
    private Label Destination2;

    @FXML
    private Label Distance2;

    @FXML
    private VBox layout_vortex;

    @FXML
    private TextArea textAreaVortex2Pc;

    @FXML
    private Label Destination3;

    @FXML
    private Label Distance3;

    @FXML
    private TextArea textAreaVortex3Pc;

    @FXML
    private TextField textFieldDepartineraire;

    @FXML
    private TextField textFieldArriveeItineraire;

    @FXML
    private GridPane layout_triangulation;

    private String typeStatistics [] = {"Tableau Moyenne","Graphe des 10 Meilleures Tirs"};

    private Popup popup =  new Popup();

    private VortexList vortexList = null;
    
    private Button lastButton  = null;

    private String pathApplication;

    private File vortexFile;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> languages = FXCollections.observableArrayList(typeStatistics);

        buttonValidationVortexProche.setOnAction(this::proximityVortexEvent);
        button_triangulation_planete.setOnAction(this::triangulationEvent);
        button_triangulation.setOnAction(Controller.this);
        button_settings.setOnAction(Controller.this);
        buttonItineraire.setOnAction(Controller.this);
        buttonVaisseau.setOnAction(Controller.this);
        button_vortex.setOnAction(Controller.this);
        lastButton =  buttonVaisseau;
        buttonAjoutFichierVortex.setOnAction(this::settingsEvent);
        choixStatistics.setItems(languages);
        buttonValidationStatistics.setOnAction(this::analyseurEvent);
        buttonCalculerRoute.setOnAction(this::itineraireEvent);
        nomVaisseau.setCellValueFactory(new PropertyValueFactory("name"));
        vaisseauDetruitEtPerdu.setCellValueFactory(new PropertyValueFactory("nbOfEnnemyShipsDestroyed"));
        bouclierMoyenne.setCellValueFactory(new PropertyValueFactory("shieldAVG"));
        BouclierShootMax.setCellValueFactory(new PropertyValueFactory("shootMaxOnShield"));
        bouclierDegatTotal.setCellValueFactory(new PropertyValueFactory("damageTotalOnShield"));
        coqueMoyenne.setCellValueFactory(new PropertyValueFactory("hullAVG"));
        coqueDegatTotal.setCellValueFactory(new PropertyValueFactory("damageTotalOnHull"));
        coqueShootMax.setCellValueFactory(new PropertyValueFactory("shootMaxOnHull"));
        degatTotalBouclierEtCoque.setCellValueFactory(new PropertyValueFactory("dammageTotal"));
        pathApplication =  System.getProperty("user.dir") + "\\data.txt";
        vortexFile = new File(pathApplication);
        if (vortexFile.exists()){
            vortexList = (Reader.readVortexFile((String)Reader.readFile(pathApplication)[1]));
        }

        System.out.println(pathApplication);



    }

    private void triangulationEvent(ActionEvent e){
        int p1,p2,p3,p11E,p12E,p13E,p21E = -1,p22E = 0,p23E=0,p31E = -1,p32E=0,p33E=0,p41E = -1,p42E=0,p43E=0,p51E = -1,p52E=0,p53E=0;
        try {
             p1 = Integer.parseInt(planet1.getText());
             p2 = Integer.parseInt(planet2.getText());
             p3 = Integer.parseInt(planet3.getText());
             p11E = Integer.parseInt(planetE11.getText());
             p12E = Integer.parseInt(planetE12.getText());
             p13E = Integer.parseInt(planetE13.getText());

             if (!planetE21.getText().isEmpty()){
                 p21E = Integer.parseInt(planetE21.getText());
                 p22E = Integer.parseInt(planetE22.getText());
                 p23E = Integer.parseInt(planetE23.getText());
             }

            if (!planetE31.getText().isEmpty()){
                p31E = Integer.parseInt(planetE31.getText());
                p32E = Integer.parseInt(planetE32.getText());
                p33E = Integer.parseInt(planetE33.getText());
            }

            if (!planetE41.getText().isEmpty()){
                p41E = Integer.parseInt(planetE41.getText());
                p42E = Integer.parseInt(planetE42.getText());
                p43E = Integer.parseInt(planetE43.getText());
            }

            if (!planetE51.getText().isEmpty()){
                p51E = Integer.parseInt(planetE51.getText());
                p52E = Integer.parseInt(planetE52.getText());
                p53E = Integer.parseInt(planetE53.getText());

            }
        }catch (NumberFormatException a){
            popup.show(Key.defaultErrorTitle,Key.wrongNumberFormat);
            return;
        }

        if (p1 < 0 || p1 > 10000 ||p2 < 0 || p2 > 10000 || p3 < 0 || p3 > 10000){
            popup.show(Key.defaultErrorTitle,Key.emptyTextField);
            return;
        }

        Point plt = new Point(p1);
        Point plt2 = new Point(p2);
        Point plt3 = new Point(p3);

        if (layout_planets.getChildren().size() != 0){
            layout_planets.getChildren().clear();
        }

        String result = Traitement.triangulate(plt,plt2,plt3,p11E,p12E,p13E);

        VBox vBox = new VBox();
        vBox.getChildren().add(new Text("Planet 1 : "));
        vBox.getChildren().add(new Text(result));
        layout_planets.getChildren().add(vBox);
        HBox.setMargin(vBox,new Insets(0,20,0,0));


        if (p21E != -1){
            vBox = new VBox();
            vBox.getChildren().add(new Text("Planet 2 : "));
            result = Traitement.triangulate(plt,plt2,plt3,p21E,p22E,p23E);
            vBox.getChildren().add(new Text(result));
            layout_planets.getChildren().add(vBox);
            HBox.setMargin(vBox,new Insets(0,20,0,0));
        }

        if (p31E != -1){
            vBox = new VBox();
            vBox.getChildren().add(new Text("Planet 3 : "));
            result = Traitement.triangulate(plt,plt2,plt3,p31E,p32E,p33E);
            vBox.getChildren().add(new Text(result));
            layout_planets.getChildren().add(vBox);
            HBox.setMargin(vBox,new Insets(0,20,0,0));

        }

        if (p41E != -1){
            vBox = new VBox();
            vBox.getChildren().add(new Text("Planet 4 : "));
            result = Traitement.triangulate(plt,plt2,plt3,p41E,p42E,p43E);
            vBox.getChildren().add(new Text(result));
            layout_planets.getChildren().add(vBox);
            HBox.setMargin(vBox,new Insets(0,20,0,0));

        }

        if (p51E != -1){
            vBox = new VBox();
            vBox.getChildren().add(new Text("Planet 5 : "));
            result = Traitement.triangulate(plt,plt2,plt3,p51E,p52E,p53E);
            vBox.getChildren().add(new Text(result));
            layout_planets.getChildren().add(vBox);
            HBox.setMargin(vBox,new Insets(0,20,0,0));

        }

    }

    private void itineraireEvent(ActionEvent e){
        int arrivee;
        int depart;
        try{
           arrivee = Integer.parseInt(textFieldArriveeItineraire.getText());
           depart = Integer.parseInt(textFieldDepartineraire.getText());
            if (textFieldArriveeItineraire.getText().isEmpty() || textFieldDepartineraire.getText().isEmpty()
                || depart < 1 || depart > 10000 || arrivee < 0 || arrivee > 10000){
                System.out.println("LEL");
                popup.show(Key.defaultErrorTitle,Key.emptyTextField);
                return;
            }
        }catch (NumberFormatException err){
            popup.show(Key.defaultErrorTitle,Key.emptyTextField);
            return;
        }
        layout_vortex.setVisible(true);
        Point a = new Point(arrivee,0,0,0);
        Point d = new Point(depart,0,0,0);
        System.out.println("Calcul en cours...");
        Object [][] route = Traitement.calculateBestRoute(vortexList,d,a);
        String destination = d.getSs() + " � " + a.getSs();
        Destination1.setText(destination);
        Destination2.setText(destination);
        Destination3.setText(destination);
        Distance1.setText("Distance : " + route[0][3] + " pc - 1 sauts");
        Distance2.setText("Distance : " + route[1][3] + " pc - 2 sauts");
        Distance3.setText("Distance : " + route[2][3] + " pc - 3 sauts");
        textAreaVortex1Pc.setText(route[0][0].toString());
        textAreaVortex2Pc.setText(route[1][0].toString() + "\n"  + route[1][1].toString());
        textAreaVortex3Pc.setText(route[2][0] + "\n" + route[2][1]
         + "\n" +route[2][2]);
    }

    private void proximityVortexEvent(ActionEvent e){
        if (!vortexFile.exists()){
            popup.show(Key.defaultErrorTitle,Key.noFileVortex);
        }
        if (textFieldRechercheVortexProche.getText().isEmpty()){
            popup.show(Key.defaultErrorTitle,Key.emptyTextField);
            return;
        }
        int ss = -1;
        try{
            ss = Integer.parseInt(textFieldRechercheVortexProche.getText());
            if (ss > 10000 || ss <= 0){
                popup.show(Key.defaultErrorTitle,Key.wrongNumberFormat);
                return;
            }
        }catch (Exception x){
            popup.show(Key.defaultErrorTitle,Key.wrongNumberFormat);
            return;
        }
        Map<Integer,List<Vortex>> allProximityVortex= Traitement.getVortexNearby(new Point(ss,0,0,0),vortexList);
        String displayPattern = "Vortex � ";
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println(allProximityVortex.size());
        for (Map.Entry<Integer, List<Vortex>> entry : allProximityVortex.entrySet()) {
            Integer key = entry.getKey();
            List<Vortex> vortexList = entry.getValue();
            stringBuilder.append(displayPattern)
                        .append(key)
                        .append("pc")
                        .append(" :\n");

            for (Vortex v: vortexList) {
                stringBuilder.append(v.getEntree().getSs())
                        .append(":")
                        .append(v.getEntree().getxUe())
                        .append(":")
                        .append(v.getEntree().getyUe())
                        .append(":")
                        .append(v.getEntree().getZue())
                        .append("\t->")
                        .append("\t")
                        .append(v.getSortie().getSs())
                        .append(":")
                        .append(v.getSortie().getxUe())
                        .append(":")
                        .append(v.getSortie().getyUe())
                        .append(":")
                        .append(v.getSortie().getZue())
                        .append("\n");

            }
            stringBuilder.append("\n");
        }

        textAreaVortexProche.setText(stringBuilder.toString());

    }

    private void analyseurEvent(ActionEvent e){
//        Dialog
        if (textareaVaisseau.getText().isEmpty()){
            popup.show(Key.defaultErrorTitle,Key.emptyTextArea);
            return;
        }else if (choixStatistics.getSelectionModel().isEmpty()){
            popup.show(Key.defaultErrorTitle,Key.emptyChoiceBox);
            return;
        }
        System.out.println(choixStatistics.getSelectionModel().getSelectedItem());

        ObservableList<Unite> uniteList = FXCollections.observableArrayList(Traitement.analyseLog(textareaVaisseau.getText()).values());

        tableauMoyenne.setVisible(false);
        grapheMeilleureShoot.setVisible(false);

        // Tableau affichage
        if (choixStatistics.getSelectionModel().getSelectedItem().equals(typeStatistics[0])){
            tableauMoyenne.setVisible(true);
            tableauMoyenne.getItems().setAll(uniteList);
//            tableauMoyenne
            // Graphe Meilleures Tirs
        }else if (choixStatistics.getSelectionModel().getSelectedItem().equals(typeStatistics[1])){
            grapheMeilleureShoot.setVisible(true);
            grapheMeilleureShoot.getData().clear();
            tableauMoyenne.getItems().clear();
            System.out.println("IN");
            List<Object[]> data = Traitement.getBestShoots(textareaVaisseau.getText(),"Coque");

            for (Object[] o: data) {
                XYChart.Series<Object, Integer> dataSeries1 = new XYChart.Series();
                dataSeries1.setName(o[0] + "(" +o[1]+")");
                // mettre 0 [0]
                dataSeries1.getData().add(new XYChart.Data<>("",(Integer) o[1]));
                System.out.println(o[0] + " - " + o[1]);
                grapheMeilleureShoot.getData().add(dataSeries1);
            }
            // Graphe dégât par camp
        }

    }

    private void settingsEvent(ActionEvent event){
      File file =  getVortexFileFromUser();
      System.out.println(file.getAbsolutePath());
      System.out.println(pathApplication);
      String contentToWriteInFile = "semaine," + new Date().toString() + "\n" + "lien," + file.getAbsolutePath();
      Writer.write(pathApplication,contentToWriteInFile);
      vortexList = Reader.readVortexFile(file.getAbsolutePath());
      
    }

    private File getVortexFileFromUser(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"));

        Stage stage = (Stage) layout_settings.getScene().getWindow();
        File selectedFile = fileChooser.showOpenDialog(layout_settings.getScene().getWindow());
        if (selectedFile != null) {
            System.out.println(selectedFile.getName());
        }
        return selectedFile;
    }

    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) event.getSource();
        if (lastButton != null) {
			lastButton.setStyle("-fx-background-color: #383535");
		}
        lastButton = button;
        button.setStyle("-fx-background-color: #AC6542");
        titrePage.setText(button.getText());
        layout_Vortex.setVisible(false);
        layout_Itineraire.setVisible(false);
        layout_settings.setVisible(false);
        layout_vaisseau.setVisible(false);
        layout_triangulation.setVisible(false);

        if (button.getText().equals(button_settings.getText())){
            layout_settings.setVisible(true);
        }else if (button.getText().equals(buttonItineraire.getText())){
            layout_Itineraire.setVisible(true);
        }else if (button.getText().equals(buttonVaisseau.getText())){
            layout_vaisseau.setVisible(true);
        }else if (button.getText().equals(button_vortex.getText())){
            layout_Vortex.setVisible(true);
        }else if (button.getText().equals(button_triangulation.getText())){
            layout_triangulation.setVisible(true);
        }
    }
}
