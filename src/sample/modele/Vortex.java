package sample.modele;

public class Vortex{
    private Point entree;
    private double distance;
    private Point sortie;

    public Vortex(Point entree, Point sortie) {
        this.entree = entree;
        this.sortie = sortie;
    }

    public Point getEntree() {
        return entree;
    }

    public Point getSortie() {
        return sortie;
    }

    @Override
    public String toString() {
        return entree + "\t->\t" + sortie;
    }

    public void setEntree(Point entree) {
        this.entree = entree;
    }

    public void setSortie(Point sortie) {
        this.sortie = sortie;
    }

    @Override
    public int hashCode() {
        return 155;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Vortex copie = (Vortex)super.clone();
        copie.setEntree(new Point(this.getEntree().getSs(),this.getEntree().getxUe(),this.getEntree().getyUe(),this.getEntree().getZue()));
        copie.setSortie(new Point(this.getSortie().getSs(),this.getSortie().getxUe(),this.getSortie().getyUe(),this.getSortie().getZue()));

        return copie;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vortex){
            Vortex v = (Vortex) obj;
//            return this.getEntree().equals(v.getEntree()) &&
//                    this.getSortie().equals(v.getSortie());
            return this.getEntree().equals(v.getEntree()) &&
                    this.getSortie().equals(v.getSortie()) ||
                    this.getEntree().equals(v.getSortie()) && this.getSortie().equals(v.getEntree());
        }
        return false;
    }


}
