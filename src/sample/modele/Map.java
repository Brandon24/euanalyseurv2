package sample.modele;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Map {
    public Map(){
    }

    private static  int distancesTable [][];
    public static int distance(int start,int end){
        if(distancesTable == null) {
            distancesTable = new int[100][100];
            for(int x = 0; x < 100; x++) {
                for(int y = 0; y < 100; y++)
                    distancesTable[x][y] = (int)Math.round(Math.sqrt(x * x + y * y));
            }
        }
        start--;
        end--;
        int startX = start % 100; // 5050 50
        int endX = end % 100; // 5149 49
        int mStartmEnd = startX - endX; // 1
        int dStartdEnd = start / 100 - end / 100;
        if(dStartdEnd < 0)
            dStartdEnd *= -1;
        if(mStartmEnd < 0)
            mStartmEnd *= -1;
        return distancesTable[mStartmEnd][dStartdEnd];
    }
}
