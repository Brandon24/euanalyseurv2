package sample.modele;

import java.util.ArrayList;
import java.util.List;

public  class Unite {
    private String name;
    private int pointDeVie = 0;
    private int shieldOfUnite = 0;

    private int nbOfEnnemyShipsDestroyed = 0;
    private int numberOfOurShipsDestroyed = 0;

    private List<Integer> listOfHullShots = new ArrayList<>();
    private List<Integer> listOfShieldShots = new ArrayList<>();


    private List<String> listOfEnnemyShipsTouched = new ArrayList<>();



    private int damageTotalOnShield = 0;
    private int numberOfShotsOnShield = 0;
    private int shootMaxOnShield = 0;
    private double shieldAVG = 0.0;


    private int damageTotalOnHull = 0;
    private int numberOfShotsOnSHull = 0;
    private int shootMaxOnHull = 0;
    private double hullAVG = 0.0;


    private int damageTotalUnderHull = 0;
    private int numberOfShotsUnderSHull = 0;
    private int shootMaxUnderHull = 0;

    private int dammageTotal = 0;



    public Unite() {
    }


    public List<Integer> getListOfHullShots() {
        return listOfHullShots;
    }
    public void addHullShotOnList(int shot){
        if (shot != 0){
            listOfHullShots.add(shot);
        }
    }
    public void addShieldShotOnList(int shot){
        if (shot != 0){
            listOfShieldShots.add(shot);
        }
    }

    public List<Integer> getListOfShieldShots() {
        return listOfShieldShots;
    }

    public double getHullAVG() {
        try{
            return getDamageTotalOnHull() / getNumberOfShotsOnSHull();
        }catch (ArithmeticException e){
            return 0;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDammageTotal() {
        return damageTotalOnHull + damageTotalUnderHull + damageTotalOnShield;
    }

    public long getShieldOfUnite() {
        return shieldOfUnite;
    }

    public void addShieldOnUnite(long shieldOfUnite) {
        this.shieldOfUnite += shieldOfUnite;
    }

    public long getPointDeVie() {
        return pointDeVie;
    }

    public void addPointDeVie(int pointDeVie) {
        this.pointDeVie = pointDeVie;
    }

    public int getDamageTotalOnShield() {
        return damageTotalOnShield;
    }

    public void addDamageTotalOnShield(int damageTotalOnShield) {
        if (damageTotalOnShield != 0){
            this.damageTotalOnShield += damageTotalOnShield;
            addOneShotOnShield();
        }
    }

    public int getNumberOfShotsOnShield() {
        return numberOfShotsOnShield;
    }

    public void addOneShotOnShield() {
        this.numberOfShotsOnShield += 1;
    }

    public int getShootMaxOnShield() {
        return shootMaxOnShield;
    }

    public void setShootMaxOnShield(int shootMaxOnShield) {
        if (this.shootMaxOnShield < shootMaxOnShield){
            this.shootMaxOnShield = shootMaxOnShield;
        }
    }

    public long getDamageTotalOnHull() {
        return damageTotalOnHull;
    }

    public void addDamageTotalOnHull(int damageTotalOnHull) {
        if (damageTotalOnHull != 0){
            this.damageTotalOnHull += damageTotalOnHull;
            addOneShotOnHull();
        }
    }

    public long getNumberOfShotsOnSHull() {
        return numberOfShotsOnSHull;
    }

    public void addOneShotOnHull() {
        this.numberOfShotsOnSHull += 1;
    }

    public int getShootMaxOnHull() {
        return shootMaxOnHull;
    }

    public void setShootMaxOnHull(int shootMaxOnHull) {
        if (this.shootMaxOnHull < shootMaxOnHull){
            this.shootMaxOnHull = shootMaxOnHull;
        }
    }

    public long getDamageTotalUnderHull() {
        return damageTotalUnderHull;
    }

    public void addDamageTotalUnderHull(int damageTotalUnderHull) {
        if (damageTotalUnderHull != 0) {
            this.damageTotalUnderHull += damageTotalUnderHull;
            addOneShotUnderSHull();
        }
    }

    public long getNumberOfShotsUnderSHull() {
        return numberOfShotsUnderSHull;
    }

    public void addOneShotUnderSHull() {
        this.numberOfShotsUnderSHull += 1;
    }

    public int getShootMaxUnderHull() {
        return shootMaxUnderHull;
    }


    public double getShieldAVG(){
        try {
            return getDamageTotalOnShield() / getNumberOfShotsOnShield();
        }catch (ArithmeticException e){
            return 0;
        }
    }

    public void setShootMaxUnderHull(int shootMaxUnderHull) {
        if (this.shootMaxUnderHull < shootMaxUnderHull){
            this.shootMaxUnderHull = shootMaxUnderHull;
        }
    }

    public List<String> getListOfEnnemyShipsTouched() {
        return listOfEnnemyShipsTouched;
    }

    public void setListOfEnnemyShipsTouched(List<String> listOfEnnemyShipsTouched) {
        this.listOfEnnemyShipsTouched = listOfEnnemyShipsTouched;
    }



    public String getName() {
        return name;
    }

    public String getNbOfEnnemyShipsDestroyed() {
        return nbOfEnnemyShipsDestroyed + " / " + numberOfOurShipsDestroyed;
    }

    public void increaseNbOfEnnemyShipsDestroyed() {
        this.nbOfEnnemyShipsDestroyed += 1;
    }

    public int getNumberOfOurShipsDestroyed() {
        return numberOfOurShipsDestroyed;
    }

    public void increaseNumberOfOurShipsDestroyed() {
        this.numberOfOurShipsDestroyed += 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof  Unite){
            Unite unite = (Unite) obj;
            return this.name.equals(unite.getName());
        }
        return false;
    }

    @Override
    public String toString() {
        return "Unite{" +
                "pointDeVie=" + pointDeVie +
                ", shieldOfUnite=" + shieldOfUnite +
                ", damageTotalOnShield=" + damageTotalOnShield +
                ", numberOfShotsOnShield=" + numberOfShotsOnShield +
                ", shootMaxOnShield=" + shootMaxOnShield +
                ", damageTotalOnHull=" + damageTotalOnHull +
                ", numberOfShotsOnSHull=" + numberOfShotsOnSHull +
                ", shootMaxOnHull=" + shootMaxOnHull +
                ", damageTotalUnderHull=" + damageTotalUnderHull +
                ", numberOfShotsUnderSHull=" + numberOfShotsUnderSHull +
                ", shootMaxUnderHull=" + shootMaxUnderHull +
                '}';
    }
}
