package sample.modele;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Point {
    private int ss;

    private int xUe;
    private int yUe;
    private int zue;

    public Point(int ss, int xUe, int yUe, int zue) {
        this.ss = ss;
        this.xUe = xUe;
        this.yUe = yUe;
        this.zue = zue;

    }
    public Point(int ss) {
        this.ss = ss;
        this.xUe = 0;
        this.yUe = 0;
        this.zue = 0;

    }

    public int getSs() {
        return ss;
    }

    public int getxUe() {
        return xUe;
    }

    public int getyUe() {
        return yUe;
    }

    public int getZue() {
        return zue;
    }

    public double calculPrecisDistanceBeetWeenTwoPoints(Point point){
//        BigDecimal big = new BigDecimal(0.00);
        int calc = (int) Math.sqrt(Math.pow(getSs() % 100 - point.getSs() % 100,2) + Math.pow(getSs() / 100 - point.getSs() / 100,2));
//        int calc = (int) calculDistanceBeetWeenTwoPoints(point);
        int calc1 = (int) Math.sqrt(Math.pow(getxUe() - point.getxUe(),2) + Math.pow(getyUe()-point.getyUe(),2) + Math.pow(getZue()-point.getZue(),2));

        return Double.parseDouble(calc+"."+calc1);
    }

    public double calculDistanceBeetWeenTwoPoints(Point point){
        return new BigDecimal(Math.sqrt(Math.pow(getSs() % 100 - point.getSs() % 100,2) + Math.pow(getSs() / 100 - point.getSs() / 100,2))).setScale(2,BigDecimal.ROUND_HALF_EVEN).doubleValue();
    }

    @Override
    public String toString() {
        return ss +
                ":" + xUe +
                ":" + yUe +
                ":" + zue;
    }


    public void setSs(int ss) {
        this.ss = ss;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof  Point){
            Point point = (Point) obj;
            return this.ss == point.getSs() &&
                    this.xUe == point.getxUe() &&
                    this.yUe == point.getyUe() &&
                    this.zue == point.getZue();
        }
        return false;
    }
}
