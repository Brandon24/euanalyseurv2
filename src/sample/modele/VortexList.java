package sample.modele;

import java.util.ArrayList;
import java.util.List;

public class  VortexList extends ArrayList<Vortex> {

    public VortexList(){
        super();
    }

    /**
     *
     * @return une liste de vortex sans doublon
     */
    public List<Vortex> getVortex(){
        int size = this.size();
        List<Vortex> allVortexSansDoublon = new ArrayList<>();

        for (Vortex v: this) {
            if (!allVortexSansDoublon.contains(v)){
                allVortexSansDoublon.add(v);
            }
        }
        System.out.println(allVortexSansDoublon.size());
        return allVortexSansDoublon;
    }
}
