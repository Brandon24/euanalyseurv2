package sample.modele;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class Statistique {
    private double percentageOnHull;
    private double percentageOnShield;
    private double percentageOnAllDammage;
    private int totalDammageOnHull;
    private int totalDammegeOnShield;
    private int totalDammage;
    private int totalDammageByAll;
    private int totalDammageOnShieldByAll;
    private int totalDammageOnHulldByAll;
    private List<String> shipsName;


    /**
     *
     * @param totalDammageOnHull is dammage caused by one camp
     * @param totalDammegeOnShield is dammage caused by one camp
     * @param totalShieldDammageByAll is dammage caused by all camp on shield
     * @param totalHullDammageByAll is dammage caused by all camp on hull
     */
    public Statistique(int totalDammageOnHull, int totalDammegeOnShield,int totalShieldDammageByAll, int totalHullDammageByAll,List<String> shipsName) {
        this.totalDammageOnHull = totalDammageOnHull;
        this.totalDammegeOnShield = totalDammegeOnShield;
        this.totalDammageByAll = totalShieldDammageByAll + totalHullDammageByAll;
        this.totalDammage = totalDammageOnHull + totalDammegeOnShield;
        this.percentageOnAllDammage = 100.0 *  this.totalDammage / this.totalDammageByAll;
        this.percentageOnHull =  100.0 * this.totalDammageOnHull/ totalHullDammageByAll;
        this.percentageOnShield = 100.0 * this.totalDammegeOnShield /totalShieldDammageByAll;
        this.totalDammageOnHulldByAll = totalHullDammageByAll;
        this.totalDammageOnShieldByAll = totalShieldDammageByAll;
        this.shipsName = shipsName;

    }

    public double getPercentageOnHull() {
        return percentageOnHull;
    }

    public double getPercentageOnShield() {
        return percentageOnShield;
    }

    public int getTotalDammageOnHull() {
        return totalDammageOnHull;
    }

    public int getTotalDammegeOnShield() {
        return totalDammegeOnShield;
    }

    public int getTotalDammageByAll() {
        return totalDammageByAll;
    }

    public double getPercentageOnAllDammage() {
        return percentageOnAllDammage;
    }

    public int getTotalDammage() {
        return totalDammage;
    }

    public int getTotalDammageOnShieldByAll() {
        return totalDammageOnShieldByAll;
    }

    public int getTotalDammageOnHulldByAll() {
        return totalDammageOnHulldByAll;
    }

    public List<String> getShipsName() {
        return shipsName;
    }

    @Override
    public String toString() {
        return "Statistique{" +
                "percentageOnHull=" + percentageOnHull +
                ", percentageOnShield=" + percentageOnShield +
                ", percentageOnAllDammage=" + percentageOnAllDammage +
                ", totalDammageOnHull=" + totalDammageOnHull +
                ", totalDammegeOnShield=" + totalDammegeOnShield +
                ", totalDammage=" + totalDammage +
                ", totalDammageByAll=" + totalDammageByAll +
                ", totalDammageOnShieldByAll=" + totalDammageOnShieldByAll +
                ", totalDammageOnHulldByAll=" + totalDammageOnHulldByAll +
                ", shipsName=" + shipsName +
                '}';
    }
}
