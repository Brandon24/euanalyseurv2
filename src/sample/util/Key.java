package sample.util;

public class Key {
    public static final String defaultErrorTitle = "Erreur";
    public static final String emptyTextArea = "Veuillez mettre le log de combat et valider après !";
    public static final String wrongNumberFormat = "Veuillez mettre un nombre décimal !";
    public static final String emptyTextField = "Veuillez entre un systéme solaire entre 1 et 10 000";
    public static final String emptyChoiceBox = "Veuillez sélectionner le type d'analyse voulu !";
    public static final String noFileVortex = "Veuillez allez dans les paramétres de l'application et ajouter le fichier qui contient les vortex!";



}
