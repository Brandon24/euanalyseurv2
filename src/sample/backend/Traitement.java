package sample.backend;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import sample.modele.*;

public class Traitement {
   private static Logger logger = Logger.getLogger(Traitement.class.toString());
   private final static String resistanceWord = "R�sistance de la coque";
   private final static String shieldWord = "Bouclier";
   private final static String detruitWord = "D�truit(s)";
   private final static String attaquesWord = "Attaques";
   private final static String dommageWord = "Dommages:";
   private final static String maniaWord = "Manoeuvrabilit�";
   private final static String fuiteWord = "Retraite";
   private final static String consitutionWord = "Points de constitution";
   static List<Vortex> allVortex = null;



    public static HashMap<String,Unite> analyseLog(String data){
        HashMap<String,Unite> uniteList = new HashMap<>();
        int positioResistanceKeyWord= 0;
        int positionShieldKeyWord = 0;
        int positionDetruitKeyWord = 0;
        int positionManiaWord = 0;
        int positionFuiteWord = 0;
        int positionConstitutionWord = 0;
        int positionDommageWord = 0;
        String currentLine = "";
        String shipName = "";
        String matched [] = data.split("\n");
        String ennemyShip = "";
        int countShot = 0;
        for (int i = 0; i <  matched.length; i++) {

            try{
                positioResistanceKeyWord = matched[i].indexOf(resistanceWord);
                positionShieldKeyWord = matched[i].indexOf(shieldWord);
                positionDetruitKeyWord = matched[i].indexOf(detruitWord);
                positionManiaWord = matched[i].indexOf(maniaWord);
                positionFuiteWord = matched[i].indexOf(fuiteWord);
                positionConstitutionWord = matched[i].indexOf(consitutionWord);
                positionDommageWord = matched[i].indexOf(dommageWord);

                if (positionDetruitKeyWord != -1){
                    uniteList.get(shipName).increaseNbOfEnnemyShipsDestroyed();
                    String shipDestroyed = matched[i].substring(0,positionDetruitKeyWord - 1);

                    if (uniteList.containsKey(shipDestroyed)){
                        uniteList.get(shipDestroyed).increaseNumberOfOurShipsDestroyed();
                    }else {
                        Unite u = new Unite();
                        u.setName(shipDestroyed);
                        u.increaseNumberOfOurShipsDestroyed();
                        uniteList.put(shipDestroyed,u);
                    }

                    continue;
                }else if (positionManiaWord != -1){
                    continue;
                }else if (positionFuiteWord != -1){
                    logger.info("Condition Fuite");
                    continue;
                }else if (positioResistanceKeyWord == -1 && positionConstitutionWord == - 1){
                    continue;
                }

                currentLine =  matched[i];
                int positionKeyWordAttaque = currentLine.indexOf(attaquesWord);
                int positionDommageKeyWordWithHisSize = positionDommageWord + dommageWord.length();
                shipName = currentLine.substring(0,positionKeyWordAttaque - 1);
                ennemyShip = currentLine.substring(positionKeyWordAttaque + attaquesWord.length() + 1, positionDommageWord - 3);

                // Pour enlever l'espace
                

                int dammageOnHull;
                int dammageUnderHull;
                int dammageOnShield;
                if (positioResistanceKeyWord != -1){
                    dammageOnHull = positionShieldKeyWord == -1 ? Integer.parseInt(currentLine.substring(positionDommageKeyWordWithHisSize + 1, positioResistanceKeyWord - 1)) : 0;
                    dammageUnderHull = positionShieldKeyWord == -1 ? 0 :Integer.parseInt(currentLine.substring(positionDommageKeyWordWithHisSize + 1, positioResistanceKeyWord - 1));
                    dammageOnShield = positionShieldKeyWord == -1 ? 0 : Integer.parseInt(currentLine.substring(positioResistanceKeyWord + resistanceWord.length() + 3, positionShieldKeyWord- 1));
                }else {
                    dammageOnHull = positionShieldKeyWord == -1 ? Integer.parseInt(currentLine.substring(positionDommageKeyWordWithHisSize + 1, positionConstitutionWord - 1)) : 0;
                    dammageUnderHull = positionShieldKeyWord == -1 ? 0 :Integer.parseInt(currentLine.substring(positionDommageKeyWordWithHisSize + 1, positionConstitutionWord - 1));
                    dammageOnShield = positionShieldKeyWord == -1 ? 0 : Integer.parseInt(currentLine.substring(positionConstitutionWord + consitutionWord.length() + 3, positionShieldKeyWord- 1));
                }
                

                Unite unite;

                if (uniteList.containsKey(shipName)){
                    unite =  uniteList.get(shipName);
                }else{
                    unite = new Unite();
                }
                if (!unite.getListOfEnnemyShipsTouched().contains(ennemyShip)){
                    unite.getListOfEnnemyShipsTouched().add(ennemyShip);
                }
                countShot++;

                unite.setName(shipName);
                unite.setShootMaxOnHull(dammageOnHull);
                unite.setShootMaxOnShield(dammageOnShield);
                unite.setShootMaxUnderHull(dammageUnderHull);
                unite.addDamageTotalOnHull(dammageOnHull);
                unite.addDamageTotalOnShield(dammageOnShield);
                unite.addDamageTotalUnderHull(dammageUnderHull);
                unite.addHullShotOnList(dammageOnHull);
//                unite.addDammageOnRound(new StatistiqueRound(countShot,
//                        dammageOnHull + dammageOnShield + dammageUnderHull));
                unite.addShieldShotOnList(dammageOnShield);
                uniteList.put(shipName,unite);

            }catch (Exception e){
                e.getStackTrace();
                System.out.println(e.getMessage());
                System.out.println( e.getLocalizedMessage());
            }

        }

        return uniteList;
    }

    /**
     *
     * @param data
     * @param type bouclier ou coque
     * @return
     */
    public static List<Object[]> getBestShoots(String data, String type){

        HashMap<String, Unite> analyse = analyseLog(data);
//        Initialiser une liste de 10 integer ^^
        List<Object[]> allShots = new ArrayList<>(10);
        int lastIndexAllShots = 9;

        for (int i = 0; i <10 ; i++) {
            allShots.add(new Object[]{"",0});
        }

        List<Integer> tempShots;
        int currentShoot;
        for (HashMap.Entry<String, Unite> entry : analyse.entrySet()) {
            String key = entry.getKey();
            Unite value = entry.getValue();
            if (value.getListOfHullShots().size() != 0){
                tempShots = value.getListOfHullShots();
                // On parcours tous les tirs de notre vaisseau sur coque
                for (int i = 0; i < tempShots.size(); i++) {
                    currentShoot = tempShots.get(i);
                    if (currentShoot > (Integer) allShots.get(lastIndexAllShots)[1]){
//                        On parcours la liste des meilleures tirs si le shoot actuel est meilleure que le dernier tir de notre liste
                        for (int j = 0; j <  allShots.size(); j++) {
                            if (currentShoot > (Integer) allShots.get(j)[1]){
                                Object[] tempLastShot =  allShots.get(j);
                                Object[] tempLastShot2;
                                allShots.set(j,new Object[]{key,currentShoot});
//                                On d�cale tous les tirs du coups
                                for (int k = j + 1; k < allShots.size() ; k++) {
                                    if ((Integer)allShots.get(k)[1]  < (Integer)tempLastShot[1]){
                                        tempLastShot2 = allShots.get(k);
                                        allShots.set(k,tempLastShot);
                                        tempLastShot = tempLastShot2;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        return allShots;
    }


    /**
     *
     * @param depart
     * @param allVortex sans dupplication de données la liste doit être passé
     * @return
     */
    public static HashMap<Integer,List<Vortex>> getVortexNearby(Point depart,List<Vortex> allVortex){
        HashMap<Integer,List<Vortex>> allVortexNearbyOfThreePc = new HashMap<>();

        Vortex vortexTemp = null;
        for (Vortex vortex : allVortex) {
            try {
                double calcDistanceWithFirstVortex = vortex.getEntree().calculDistanceBeetWeenTwoPoints(depart);
                double calcDistanceWithSecondVortex = vortex.getSortie().calculDistanceBeetWeenTwoPoints(depart);
                int distance = -1;
                vortexTemp = new Vortex(vortex.getEntree(), vortex.getSortie());
//                System.out.println("Clone effected" + vortex);


                if (calcDistanceWithFirstVortex >= 1.0 && calcDistanceWithFirstVortex < 4.0) {
                    distance = (int) calcDistanceWithFirstVortex;
                } else if (calcDistanceWithSecondVortex >= 1.0 && calcDistanceWithSecondVortex < 4.0) {
                    distance = (int) calcDistanceWithSecondVortex;
                    Point temp = vortexTemp.getEntree();
                    vortexTemp.setEntree(vortexTemp.getSortie());
                    vortexTemp.setSortie(temp);
                }

                if (distance != -1) {
                    List<Vortex> vList = allVortexNearbyOfThreePc.containsKey(1) ? allVortexNearbyOfThreePc.get(distance) : new ArrayList<>();
                    vList.add(vortexTemp);
                    allVortexNearbyOfThreePc.put(distance, vList);
                }
            }catch (Exception e){

            }
        }

        return allVortexNearbyOfThreePc;
    }



    public static Object[][] calculateBestRoute(VortexList vortexes, Point depart, Point arrive){
    	Double minDistance = arrive.calculDistanceBeetWeenTwoPoints(depart);
        int minDistanceToSearchProximityVortex = 7;

        List<Vortex> vDepart = new ArrayList<>();
        List<Vortex> vArrivee = new ArrayList<>();
        // On calcule les vortex les plus proches
        double calc, calc1;
        for (Vortex v: vortexes) {
            calc = sample.modele.Map.distance(v.getEntree().getSs(),depart.getSs());
            
            calc1 = sample.modele.Map.distance(v.getSortie().getSs(),depart.getSs());

            if (v.getEntree().getSs() == depart.getSs() || v.getSortie().getSs() == arrive.getSs()
            		|| v.getSortie().getSs() == depart.getSs() || v.getEntree().getSs() == arrive.getSs()){
                continue;
            }

            if (calc <= minDistanceToSearchProximityVortex){
                vDepart.add(v);
            }
            
            if (calc1 <= minDistanceToSearchProximityVortex ){
                vDepart.add(new Vortex(v.getSortie(),v.getEntree()));
            }

            calc =  sample.modele.Map.distance(v.getEntree().getSs(),arrive.getSs());
            calc1 = sample.modele.Map.distance(v.getSortie().getSs(),arrive.getSs());

            if (calc1 <= minDistanceToSearchProximityVortex){
                vArrivee.add(v);
            }
            if (calc <= minDistanceToSearchProximityVortex){
                vArrivee.add(new Vortex(v.getSortie(),v.getEntree()));
            }
        }
        
        Object finalRoutes [][] = new Object[3][4];
        double minDistanceFor3Pc = 6.0;
        double temp = 100;
        for (Vortex v: vortexes) {
            for (Vortex vovoDepart: vDepart) {
                for (Vortex vovoArrivee: vArrivee) {
                	// fast dist
                    if ( sample.modele.Map.distance(vovoDepart.getSortie().getSs(),v.getEntree().getSs()) + sample.modele.Map.distance(v.getSortie().getSs(),vovoArrivee.getEntree().getSs()) < minDistanceFor3Pc){
                        calc = depart.calculDistanceBeetWeenTwoPoints(vovoDepart.getEntree()) +
                                vovoDepart.getSortie().calculDistanceBeetWeenTwoPoints(v.getEntree()) +
                                v.getSortie().calculDistanceBeetWeenTwoPoints(vovoArrivee.getEntree()) +
                                vovoArrivee.getSortie().calculDistanceBeetWeenTwoPoints(arrive);
                        //&& vovoArrivee.getSortie().getSs() != arrive.getSs()
                        if (calc < temp && !v.equals(vovoArrivee) && vovoDepart.getSortie().getSs() != v.getEntree().getSs()  && v.getSortie().getSs() != vovoArrivee.getEntree().getSs()){
                            temp = calc;
                            finalRoutes[2][0] = vovoDepart;
                            finalRoutes[2][1] =  v;
                            finalRoutes[2][2] =  vovoArrivee;
                            finalRoutes[2][3] =  calc;
                        }

                    } if (sample.modele.Map.distance(vovoDepart.getSortie().getSs(),v.getSortie().getSs()) + sample.modele.Map.distance(v.getEntree().getSs(),vovoArrivee.getEntree().getSs()) < minDistanceFor3Pc){
                        calc = depart.calculDistanceBeetWeenTwoPoints(vovoDepart.getEntree()) +
                                vovoDepart.getSortie().calculDistanceBeetWeenTwoPoints(v.getSortie()) +
                                v.getEntree().calculDistanceBeetWeenTwoPoints(vovoArrivee.getEntree()) +
                                vovoArrivee.getSortie().calculDistanceBeetWeenTwoPoints(arrive);
                        //&& vovoArrivee.getEntree().getSs() != arrive.getSs()
                        if (calc < temp && ! v.equals(vovoArrivee) && vovoDepart.getSortie().getSs() != v.getSortie().getSs()&& v.getEntree().getSs() != vovoArrivee.getEntree().getSs()){
                            temp = calc;
                            finalRoutes[2][0] = vovoDepart;
                            finalRoutes[2][1] =  new Vortex(v.getSortie(),v.getEntree());
                            finalRoutes[2][2] =  vovoArrivee;
                            finalRoutes[2][3] =  calc;
                        }
                  
                    }
                }
            }

        }



        // On calcule la route la plus courte
        double minDist = minDistance;
        for (Vortex vD: vDepart) {
            calc1 = vD.getEntree().calculDistanceBeetWeenTwoPoints(depart) + vD.getSortie().calculDistanceBeetWeenTwoPoints(arrive);
            // Best route for one PC
            if (calc1 < minDist){
                minDist = calc1;
                finalRoutes[0][0] =  vD;
                finalRoutes[0][3] =  calc1;
            }

            for (Vortex vA:vArrivee) {
             
                if (vD.getSortie().getSs() == vA.getEntree().getSs() ){
                    continue;
                }
                calc = vD.getSortie().calculDistanceBeetWeenTwoPoints(vA.getEntree());
                calc += vA.getSortie().calculDistanceBeetWeenTwoPoints(arrive) + vD.getEntree().calculDistanceBeetWeenTwoPoints(depart) ;
                // Best route for 2 PC
                if (calc < minDistance){

                    minDistance = calc;
                    finalRoutes[1][0] =  vD;
                    finalRoutes[1][1] =  vA;
                    finalRoutes[1][3] =  calc;
                   
                }
              
            }
        }

        NumberFormat nf = new DecimalFormat("0.##");
        
   
        finalRoutes[1][3] = finalRoutes[1][3] != null ? nf.format(finalRoutes[1][3]) : 0;
        finalRoutes[2][3] = finalRoutes[2][3] != null ? nf.format(finalRoutes[2][3]) : 0;
        finalRoutes[0][3] = finalRoutes[0][3] != null ? nf.format(finalRoutes[0][3]) : 0;
        finalRoutes[0][0] = finalRoutes[0][0] != null ? finalRoutes[0][0] : "Aucune route trouv�!";
        finalRoutes[1][0] = finalRoutes[1][0] != null ? finalRoutes[1][0] : "Aucune route trouv�!";
        finalRoutes[1][1] = finalRoutes[1][1] != null ? finalRoutes[1][1] : "";
        finalRoutes[2][0] = finalRoutes[2][0] != null ? finalRoutes[2][0] : "Aucune route trouv� ! ";
        finalRoutes[2][1] = finalRoutes[2][1] != null ? finalRoutes[2][1] : "";
        finalRoutes[2][2] = finalRoutes[2][2] != null ? finalRoutes[2][2] : "";
        

        return finalRoutes;

    }

    public static String triangulate(Point a, Point b, Point c, int distA,int distB,int distC){
        String result = "";
        Point temp;
        for (int i = 1; i <= 100; i++) {
            for (int j = 1; j <= 100; j++) {
                temp = new Point(getSys(i,j),0,0,0);

                if (Math.round(a.calculDistanceBeetWeenTwoPoints(temp)) == distA &&  Math.round(b.calculDistanceBeetWeenTwoPoints(temp)) == distB && Math.round(c.calculDistanceBeetWeenTwoPoints(temp)) == distC){
                    result += temp.getSs() + "\n";
                }

            }
        }

        return result == "" ? "Not found" : result;
    }

    private static int getSys(int x, int y){
        int sys;
        x=100*x;
        if(y==100){
            sys = x;
        }
        else sys = x-100+y;
        return sys;

    }
}


