package sample.writer;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Writer {

    public static void write(String pathOnYourDisk,String dataToWrite) {
        File file = new File(pathOnYourDisk);
        java.io.Writer writer;
        try {
//            writer = new FileWriter(file);
            writer =  new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
            writer.write(dataToWrite);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
