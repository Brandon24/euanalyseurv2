package sample.component;

import javafx.scene.control.Alert;

public class Popup {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    public void show(String titre,String message){
        alert.setTitle(titre);
        // Header Text: null
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
