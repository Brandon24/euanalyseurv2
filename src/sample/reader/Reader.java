package sample.reader;

import sample.modele.Point;
import sample.modele.Vortex;
import sample.modele.VortexList;
import sample.writer.Writer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reader {

    public static Object[] readFile(String pathOfCsvFile){
        String typeVortex = "\"Entrée de Vortex\",";
        Object object [] = new Object[2];

        try (BufferedReader br = new BufferedReader(new FileReader(pathOfCsvFile))) {
            String line;
            String[] values;
            while ((line = br.readLine()) != null) {
                values = line.split(",");
                if (values[0].equals("semaine")){
                    object[0] = values[1];
                }else {
                    object[1] = values[1];
                    break;
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Erreur chemin Fichier ! ");

        }
        return object;
    }
    public static VortexList readVortexFile(String pathOfCsvFile){
        String typeVortex = "\"Entrée de Vortex\",";
        System.out.println(typeVortex);
        VortexList vortexesList = new VortexList();
        Vortex vortex = null;
        try (BufferedReader br = new BufferedReader(new FileReader(pathOfCsvFile))) {
            String line;
            String vortexEntree[];
            String vortexSortie[];
            String[] values;
            while ((line = br.readLine()) != null) {
                if (line.indexOf(typeVortex) != -1){
                     values = line.split(",");
                     vortexEntree = values[1].split("-");
                     vortexSortie = values[2].split("-");
                     vortex = new Vortex(
                            new Point(Integer.parseInt(vortexEntree[0]),Integer.parseInt(vortexEntree[1]),Integer.parseInt(vortexEntree[2]),Integer.parseInt(vortexEntree[3])),
                            new Point(Integer.parseInt(vortexSortie[0]),Integer.parseInt(vortexSortie[1]),Integer.parseInt(vortexSortie[2]),Integer.parseInt(vortexSortie[3]))
                     );

                    if (!vortexesList.contains(vortex)){
                        vortexesList.add(vortex);
                    }}
                }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            String path = Paths.get(".").toAbsolutePath().normalize().toString() + "\\error.txt";
            Writer.write(path,e.getMessage() + "\n" + e.toString());
            System.err.println("Erreur chemin Fichier ! ");

        }
        return vortexesList;
    }

    public static String read(String pathOfCsvFile){
        String typeVortex = "\"Entrée de Vortex\",";
        Object object [] = new Object[2];
        String line = "";


        try (BufferedReader br = new BufferedReader(new FileReader(pathOfCsvFile))) {
            while ((line = br.readLine()) != null) {
                line+= line;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Erreur chemin Fichier ! ");

        }
        return line;
    }

}
