/**
 * 
 */
package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sample.backend.Traitement;
import sample.modele.Point;
import sample.modele.Vortex;
import sample.modele.VortexList;
import sample.reader.Reader;

/**
 * @author Brandon
 *
 */
class TestTraitement {

	
	VortexList vortexList;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		vortexList = Reader.readVortexFile("C:\\Users\\Brandon\\Downloads\\export.txt");
		
	}

	
	
	@Test
	void testCalculateBestRoute() {
		Random random = new Random();
		Point ssDepart = new Point(0);
		Point ssArrivee = new Point(0);
		int totalIterration = 2500;
		Object tempResults [][];
		Object tempResults2 [][];
		Vortex temp;
		Vortex temp2;
		
		for (int i = 0; i < totalIterration; i++) {
			ssDepart.setSs( 1 + random.nextInt( 10000 - 1));
			ssArrivee.setSs(1 + random.nextInt( 10000 - 1 ));
			tempResults = Traitement.calculateBestRoute(vortexList, ssDepart, ssArrivee);
 		    tempResults2 = Traitement.calculateBestRoute(vortexList, ssArrivee, ssDepart);
			System.out.println(ssDepart.getSs() + " - " + ssArrivee.getSs());
			System.out.println("Essai : " + i);
			System.out.println("2 vovo : " + tempResults[1][3] + " - " + tempResults2[1][3]);
			System.out.println(tempResults[1][0].toString() + "\n"  + tempResults[1][1].toString());
			System.out.println(tempResults2[1][0].toString() + "\n"  + tempResults2[1][1].toString());
//			System.out.println();
//			System.out.println(ssDepart.getSs() + " - " + ssArrivee.getSs());
//			System.out.println("3 vovo : " + tempResults[2][3] + " - " + tempResults2[2][3]);
//	        System.out.println(tempResults[2][0] + "\n" + tempResults[2][1] + "\n" +tempResults[2][2]);
//	        System.out.println();
//	        System.out.println(tempResults2[2][0] + "\n" + tempResults2[2][1] + "\n" + tempResults2[2][2]);
	        System.out.println();
	        try {
	            temp = (Vortex) tempResults2[2][0];
		        temp2 = (Vortex) tempResults2[2][1];
		
		        assertNotSame(temp.getSortie().getSs(), temp2.getEntree().getSs());
	            temp = (Vortex) tempResults2[2][1];
		        temp2 = (Vortex) tempResults2[2][2];
		        assertNotSame(temp.getSortie().getSs(), temp2.getEntree().getSs());
		        
				assertEquals(tempResults[2][3], tempResults2[2][3]);
				assertEquals(tempResults[1][3], tempResults2[1][3]);
	        }catch (Exception e) {
//	        	   (Vortex) tempResults2[1][0];
//			        (Vortex) tempResults2[1][1];
			}
	
				
			
		}
		
		
	}
	
	
//	@Test
//	void test() {
//		fail("Not yet implemented"); // TODO
//	}

}
